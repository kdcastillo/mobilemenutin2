package com.example.acer_pc.mobilemenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FoodEntryActivity extends AppCompatActivity {
    TextView welcome, info;
    EditText food, desc, price, quant;
    Button addButton, toMenu;
    String foodname, fooddesc, foodprice, foodqty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.foodentry_main);

        welcome = (TextView) (findViewById(R.id.welcomeView));
        info = (TextView) (findViewById(R.id.infoView));

        food = (EditText) (findViewById(R.id.foodEntry));
        desc = (EditText) (findViewById(R.id.descEntry));
        price = (EditText) (findViewById(R.id.priceEntry));
        quant = (EditText) (findViewById(R.id.qtyEntry));

        addButton = (Button) (findViewById(R.id.addButton));
        toMenu = (Button) (findViewById(R.id.toMenu));

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (food!=null && desc!=null && price!=null && quant!=null){
                    foodname = food.getText().toString();
                    fooddesc = desc.getText().toString();
                    foodprice = price.getText().toString();
                    foodqty = quant.getText().toString();
                    Intent toAdd = new Intent(getApplicationContext(), AddToMenuActivity.class);
                    toAdd.putExtra("food_name", foodname);
                    toAdd.putExtra("food_desc", fooddesc);
                    toAdd.putExtra("food_price", foodprice);
                    toAdd.putExtra("food_qty", foodqty);
                    startActivity(toAdd);
                    finish();

                }

            }
        });

        toMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toMenu = new Intent(getApplicationContext(), ViewMenu.class);
                startActivity(toMenu);
            }
        });

    }

}
